# FourStream

[![Laravel](https://img.shields.io/badge/Laravel-~5.1-orange.svg?style=flat-square)](http://laravel.com)
[![Latest Version](https://img.shields.io/badge/Version-~0.3-green.svg?style=flat-square)](https://github.com/ynievesdotnet/fourstream/releases)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](docs/LICENCE.md)
[![Build Status](https://img.shields.io/travis/ynievesdotnet/fourstream/master.svg?style=flat-square)](https://travis-ci.org/ynievesdotnet/fourstream)
[![Total Downloads](https://img.shields.io/packagist/dt/ynievesdotnet/fourstream.svg?style=flat-square)](https://packagist.org/packages/ynievesdotnet/fourstream)

- [Introduction](#introduction)
- [Changelog](CHANGELOG.md)
- [Licence](docs/LICENCE.md)
- [Install](docs/Install.md)
- [Configuring the Nodes](docs/Configuring.md)
- [Running the Server](docs/Running.md)
- [Sending Messages](docs/Sending.md)

<a name="introduction"></a>
## Introduction
FourStream (4Stream) is a Laravel 5.1.x Websocket Server & Client, using as Websocket Library the HOA Project.

With this project is possible send broadcast message at all user connected at WebSocket server, at one group of users or at one user specifics. See the [Examples](examples/) directory.

This project has been released using [MIT Licence](docs/LICENCE.md).

CopyRight YnievesDotNet 2007-2015
